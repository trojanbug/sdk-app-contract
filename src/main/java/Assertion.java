public interface Assertion {
    public AssertionMeta getMeta();
}
