import java.util.Optional;

public interface AssertionConsumerRegistry {
    public <A extends Assertion> AssertionConsumer<? extends A>  getAssertionConsumerFor(A assertion, Optional context);

    public static AssertionConsumerRegistry getInstance() {
        return null; // default instance provided by SDK ?
    }
}
