import java.util.Optional;

@FunctionalInterface
public interface AssertionConsumer<A extends Assertion> {
   public void assertion(A assertion, Optional<Object> context);
}
