import java.util.concurrent.Future;

@FunctionalInterface
public interface ChallengeHandler<C extends Challenge, A extends Assertion> {
    public void handle(C challenge, Object context, AssertionConsumer<A> assertionCallback, ErrorHandler<C> errorHandler);
}
