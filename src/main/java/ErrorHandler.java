public interface ErrorHandler<C extends Challenge> {
    public void handle(ChallengeError error, C challenge, Object context);
}
