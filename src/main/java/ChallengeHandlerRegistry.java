public interface ChallengeHandlerRegistry {
    public <C extends Challenge> ChallengeHandler<? extends C,?> getChallengeHandlerFor(C challenge, Object context);
}
